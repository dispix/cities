const faker = require('faker')

const cities = [
    { id: 0, name: 'lyon'},
    { id: 1, name: 'paris' },
    { id: 2, name: 'marseille' },
    { id: 3, name: 'toulouse' },
    { id: 4, name: 'lille' },
]

class Product {
    constructor() {
        this.id = faker.random.uuid()
        this.ref = faker.finance.account()
        this.name = faker.commerce.productName()
        this.price = faker.commerce.price()
        this.city = faker.random.arrayElement(cities)
    }

    toString() {
        return JSON.stringify({
            id: this.id,
            ref: this.ref,
            name: this.name,
            price: this.price,
            city: this.city,
        })
    }
}

function getData(n) {
    const products = []

    for (let i = 0; i < n; i++) {
        products.push(new Product())
    }

    return { cities, products }
}

module.exports = () => getData(30)
