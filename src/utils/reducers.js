import { Map, fromJS } from 'immutable'

/**
 *  The standard initial state for the standard reducer
 *  @type  {immutable.Map}
 */
export const standardInitialState = Map({
  loading: false,
  result: null,
  error: null,
})

/**
 *  Creates a default reducer that describe a standard http request lifecycle.
 *  @param   {Object}    SCOPE     the action scope to listen to
 *  @param   {Array}     RESETS    a list of reset constants
 *  @param   {Array}     CONTEXTS  a list of context that includes this reducer
 *  @return  {function}            a standard redux reducer
 */
export function makeStandardReducer(SCOPE, RESETS) {
  return function standardReducer(state = standardInitialState, action) {
    // Early return on the RESETS action
    if (RESETS.includes(action.type)) return standardInitialState

    switch (action.type) {
      case SCOPE.START:
        return standardInitialState.set('loading', true)
      case SCOPE.SUCCESS:
        return state.merge({
          loading: false,
          result: fromJS(action.payload.result),
        })
      case SCOPE.ERROR:
        return state.merge({
          loading: false,
          error: action.error,
        })
      default:
        return state
    }
  }
}
