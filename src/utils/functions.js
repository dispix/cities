/**
 *  Creates a function that pipes an input through each function passed in argument.
 *  This is useful for serializing transformations without having to enter a "callback"
 *  hell style of design.
 *  @param   {Iterable}  fns  Functions that will be used to transform the data
 *  @return  {Function}
 */
export function pipe(...fns) {
  return fns.reduceRight((a, b) => (...c) => a(b(...c)))
}

/**
 *  Same as `pipe` but applies the functions in the reverse order
 *  @param   {Iterable}  fns  Functions that will be used to transform the data
 *  @return  {Function}
 */
export function pipeRight(...fns) {
  return fns.reduce((a, b) => (...c) => a(b(...c)))
}
