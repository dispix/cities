/**
 *  Creates a action wrapper that will add the context to the action.
 *  @param   {String}    context  context to add in the action
 *  @return  {Function}
 */
export function contextualize(context) {
  return action => ({
    ...action,
    meta: { ...action.meta, context },
  })
}
