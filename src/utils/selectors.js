import { createSelector } from 'reselect'

/**
 *  Creates a standard selector given a domain and a path. This is useful in combination
 *  with the `makeStandardReducer` utility to quickly access a status reducer.
 *  @param   {Function}  domainSelector  A function that returns a selector to the
 *                                       Domain
 *  @param   {Array}     [path=[]]       The path in the domain to access
 *  @return  {Function}
 */
export function makeStandardSelector(domainSelector, path = []) {
  /**
   *  Returns a selector to the type given
   *  @param   {String}  type  The key to access in the substate
   *  @return  {Object}
   */
  return function standardStatusSelector(type) {
    return createSelector(domainSelector, substate =>
      substate.getIn([...path, type])
    )
  }
}
