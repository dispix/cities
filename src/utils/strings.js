/**
 *  Forces the first letter of a word/sentence to be uppercase
 *  @param   {String}  string
 *  @return  {String}
 */
export function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}
