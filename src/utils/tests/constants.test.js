import * as constants from '../constants'

describe('createStandardConstants', () => {
  const { createStandardConstants } = constants
  const PREFIX = 'TEST_SUFFIX'
  const set = createStandardConstants(PREFIX)

  it('should return a hashmap with three actions', () => {
    const expected = {
      START: `${PREFIX}.START`,
      SUCCESS: `${PREFIX}.SUCCESS`,
      ERROR: `${PREFIX}.ERROR`,
    }
    expect(set).toEqual(expected)
  })
})
