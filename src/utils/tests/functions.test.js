import * as functions from '../functions'

describe('pipe', () => {
  it('should create a function piping each functions passed', () => {
    const a = jest.fn(value => value * 2)
    const b = jest.fn(value => value * 2)
    const c = jest.fn(value => value * 2)
    const pipe = functions.pipe(a, b, c)
    const result = pipe(1)

    expect(pipe).toBeInstanceOf(Function)
    expect(a).toHaveBeenCalledWith(1)
    expect(b).toHaveBeenCalledWith(2)
    expect(c).toHaveBeenCalledWith(4)
    expect(result).toEqual(8)
  })
})

describe('pipeRight', () => {
  it('should create a function piping each functions passed, in reverse order', () => {
    const a = jest.fn(value => value * 2)
    const b = jest.fn(value => value * 2)
    const c = jest.fn(value => value * 2)
    const pipe = functions.pipeRight(a, b, c)
    const result = pipe(1)

    expect(pipe).toBeInstanceOf(Function)
    expect(c).toHaveBeenCalledWith(1)
    expect(b).toHaveBeenCalledWith(2)
    expect(a).toHaveBeenCalledWith(4)
    expect(result).toEqual(8)
  })
})
