import Promise from 'bluebird'

import request from '../request'

describe('request', () => {
  // Before each test, stub the fetch function
  beforeEach(() => {
    window.fetch = jest.fn()
  })

  describe('stubbing successful response', () => {
    it('should format the response correctly', done => {
      const res = new Response('{"hello":"world"}', {
        status: 200,
        headers: {
          'Content-type': 'application/json',
        },
      })
      fetch.mockReturnValueOnce(Promise.resolve(res))

      request('/thisurliscorrect')
        .catch(done)
        .then(json => {
          expect(json.hello).toBe('world')
          done()
        })
    })
    it('should format a bodyless response correctly', done => {
      const res = new Response(null, {
        status: 202,
        headers: {},
      })
      fetch.mockReturnValueOnce(Promise.resolve(res))

      request('/thisurliscorrect')
        .catch(done)
        .then(json => {
          expect(Object.keys(json).length).toEqual(0)
          done()
        })
    })
  })

  describe('stubbing error response', () => {
    // Before each test, pretend we got an unsuccessful response
    beforeEach(() => {
      const res = new Response('', {
        status: 404,
        statusText: 'Not Found',
        headers: {
          'Content-type': 'application/json',
        },
      })

      fetch.mockReturnValueOnce(Promise.resolve(res))
    })

    it('should catch errors', done => {
      request('/thisdoesntexist').catch(err => {
        expect(err.response.status).toBe(404)
        expect(err.response.statusText).toBe('Not Found')
        done()
      })
    })
  })
})
