import { Map } from 'immutable'

import * as reducers from '../reducers'

describe('makeStandardReducer', () => {
  const { makeStandardReducer, standardInitialState } = reducers
  const SCOPE = {
    START: 'SCOPE.START',
    SUCCESS: 'SCOPE.SUCCESS',
    ERROR: 'SCOPE.ERROR',
  }
  const RESETS = ['RESET1', 'RESET2']
  const standardReducer = makeStandardReducer(SCOPE, RESETS)

  it('should return a function', () => {
    expect(standardReducer).toBeInstanceOf(Function)
  })

  describe('standard reducer', () => {
    it('should return the standard initial state on any RESET action', () => {
      const state = Map({})
      RESETS.forEach(RESET => {
        const action = { type: RESET }
        expect(standardReducer(state, action)).toEqual(standardInitialState)
      })
    })
    describe('on SCOPE.START', () => {
      const state = Map({})
      const action = { type: SCOPE.START }
      it('should set the `loading` key to `true`', () => {
        expect(standardReducer(state, action).get('loading')).toEqual(true)
      })
      it('should set the `result` key to `null`', () => {
        expect(standardReducer(state, action).get('result')).toEqual(null)
      })
      it('should set the `error` key to `null`', () => {
        expect(standardReducer(state, action).get('error')).toEqual(null)
      })
    })
    describe('on SCOPE.SUCCESS', () => {
      const state = Map({})
      const action = { type: SCOPE.SUCCESS, payload: { result: 'test' } }
      it('should set the `loading` key to `false`', () => {
        expect(standardReducer(state, action).get('loading')).toEqual(false)
      })
      it('should save the `payload.result` key in the `result` key', () => {
        expect(standardReducer(state, action).get('result')).toEqual('test')
      })
    })
    describe('on SCOPE.ERROR', () => {
      const state = Map({})
      const action = { type: SCOPE.ERROR, error: 'error' }
      it('should set the `loading` key to `false`', () => {
        expect(standardReducer(state, action).get('loading')).toEqual(false)
      })
      it('should store the error in the `error` key', () => {
        expect(standardReducer(state, action).get('error')).toEqual('error')
      })
    })
    describe('on unknown action type', () => {
      const state = Map({})
      it('should return the state', () => {
        const action = { type: 'WRONG' }
        expect(standardReducer(state, action)).toEqual(state)
      })
    })
  })
})
