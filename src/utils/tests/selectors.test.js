import { fromJS } from 'immutable'

import * as selectors from '../selectors'

describe('makeStandardSelector', () => {
  const { makeStandardSelector } = selectors
  const DOMAIN = 'testDomain'
  const path = ['status']
  const domainSelector = jest.fn(() => state => state.get(DOMAIN))

  const standardSelector = makeStandardSelector(domainSelector(), path)
  const flatStandardSelector = makeStandardSelector(domainSelector())

  it('should return a function', () => {
    expect(standardSelector).toBeInstanceOf(Function)
  })

  it('should return a function when no path is specified', () => {
    expect(flatStandardSelector).toBeInstanceOf(Function)
  })

  describe('standardSelector', () => {
    const statuses = {
      fetch: 'fetch status',
      update: 'update status',
    }

    it("should select the right substate given it's key", () => {
      const state = fromJS({
        [DOMAIN]: {
          [path]: statuses,
        },
      })

      Object.entries(statuses).forEach(([key, value]) => {
        expect(standardSelector(key)(state)).toEqual(value)
      })
    })

    it("should select the right substate given it's key when no path is specified", () => {
      const state = fromJS({
        [DOMAIN]: statuses,
      })

      Object.entries(statuses).forEach(([key, value]) => {
        expect(flatStandardSelector(key)(state)).toEqual(value)
      })
    })
  })
})
