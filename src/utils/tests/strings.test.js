import { capitalize } from '../strings'

describe('capitalize', () => {
  it('should transform the first character to uppercase', () => {
    expect(capitalize('test')).toEqual('Test')
    expect(capitalize('Test2')).toEqual('Test2')
  })
})
