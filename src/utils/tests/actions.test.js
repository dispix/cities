import * as actions from '../actions'

describe('contextualize', () => {
  const { contextualize } = actions
  const CONTEXT = 'context'
  const withContext = contextualize(CONTEXT)

  it('should return a function', () => {
    expect(withContext).toBeInstanceOf(Function)
  })
  describe('returned function', () => {
    it('should add the context in the meta', () => {
      expect(withContext({ type: 'test' })).toEqual({
        type: 'test',
        meta: { context: CONTEXT },
      })
    })
    it('should not overwrite the whole meta key', () => {
      const action = { type: 'test', meta: { otherKey: 'other' } }
      const expected = {
        type: 'test',
        meta: { otherKey: 'other', context: CONTEXT },
      }
      expect(withContext(action)).toEqual(expected)
    })
  })
})
