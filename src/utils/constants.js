/**
 * Creates a standard set of actions, used for API calls.
 * 
 * @export
 * @param   {String} PREFIX Namespaces the actions
 * @returns {Object}
 */
export function createStandardConstants(PREFIX) {
  return {
    START: `${PREFIX}.START`,
    SUCCESS: `${PREFIX}.SUCCESS`,
    ERROR: `${PREFIX}.ERROR`,
  }
}
