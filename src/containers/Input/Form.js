import React from 'react'
import PropTypes from 'prop-types'
import Button from 'material-ui/Button'
import styled from 'styled-components'
import { Field, reduxForm } from 'redux-form/immutable'

import Input from 'components/Input'

import { FORM } from './constants'

const Form = styled.form`
  width: 300px;
  padding: 8px;
  margin: auto;
  display: flex;
  flex-direction: column;
`

const SubmitButton = styled(Button)`
  width: ${2 ** 7}px;
  margin: 8px 0;
  align-self: flex-end;
  background-color: ${({ theme }) => theme.colors.main.primary} !important;
  color: ${({ theme, disabled }) =>
    !disabled && theme.colors.main.text} !important;

  &:hover {
    background-color: ${({ theme }) => theme.colors.main.light} !important;
  }
`

const InputForm = ({ handleSubmit, pristine }) => (
  <Form onSubmit={handleSubmit}>
    <Field
      required
      label="Cities"
      name={FORM.FIELDS.CITIES}
      component={Input}
      type="text"
    />
    <SubmitButton raised type="submit" disabled={pristine}>
      OK
    </SubmitButton>
  </Form>
)

InputForm.propTypes = {
  // @reduxForm
  pristine: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
}

const createForm = reduxForm({
  form: FORM.NAME,
})

export default createForm(InputForm)
