import React from 'react'
import PropTypes from 'prop-types'
import { List } from 'immutable'
import { connect } from 'react-redux'
import { change } from 'redux-form/immutable'

import { pipeRight } from 'utils/functions'
import { register } from 'containers/App/actions'
import { AVAILABLE_CITIES } from 'containers/App/constants'

import Form from './Form'
import { FORM } from './constants'

const Input = ({ register }) => <Form onSubmit={register} />

Input.propTypes = {
  // @connect
  register: PropTypes.func.isRequired,
}

/**
 * Filters out words that are not in the cities list and enforce lowercase
 * 
 * @param {String} string 
 * @returns {List<String>}
 */
function getCities(string) {
  const toLowerCase = string => string.toLowerCase()
  const excludeUnknown = input => AVAILABLE_CITIES.includes(input)

  return List(string.split(' '))
    .map(toLowerCase)
    .filter(excludeUnknown)
}

function mapDispatchToProps(dispatch) {
  return {
    register: fields => {
      const cities = getCities(fields.get(FORM.FIELDS.CITIES))

      // Saves the cities list in the app state
      pipeRight(dispatch, register)(cities)
      // Updates the form to removes the non-city words
      pipeRight(dispatch, change)(
        FORM.NAME,
        FORM.FIELDS.CITIES,
        cities.join(' ')
      )
    },
  }
}

export default connect(null, mapDispatchToProps)(Input)
