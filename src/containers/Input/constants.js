/*
 *
 *  containers/Input constants
 *
 */

export const FORM = {
  NAME: 'citiesForm',
  FIELDS: {
    CITIES: 'cities',
  },
}
