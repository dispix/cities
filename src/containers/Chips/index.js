import React from 'react'
import PropTypes from 'prop-types'
import Button from 'material-ui/Button'
import MuiChip from 'material-ui/Chip'
import styled from 'styled-components'
import { List } from 'immutable'
import { getFormValues, change } from 'redux-form/immutable'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import { pipeRight } from 'utils/functions'
import { capitalize } from 'utils/strings'
import { list } from 'services/products/actions'
import { remove } from 'containers/App/actions'
import { selectCities } from 'containers/App/selectors'
import { FORM } from 'containers/Input/constants'

const Container = styled.div`
  display: flex;
  justify-items: center;
  align-items: center;
  width: fit-content;
  margin: auto;
`

const ChipsList = styled.div`
  display: flex;
  padding: 8px;
  margin: 0 8px;
  list-style-type: none;
`

const Chip = styled(MuiChip)`
  margin: 8px;
  background-color: ${({ theme }) => theme.colors.secondary.primary} !important;
  color: ${({ theme }) => theme.colors.secondary.text} !important;
`

const Search = styled(Button)`
  height: 36px;
  background-color: ${({ theme }) => theme.colors.main.primary} !important;
  color: ${({ theme }) => theme.colors.main.text} !important;
`

const Chips = ({ cities, onRemove, search }) => (
  <Container>
    <ChipsList>
      {cities.map((city, index) => (
        <li key={index}>
          <Chip
            onRequestDelete={() => onRemove(city)}
            label={capitalize(city)}
          />
        </li>
      ))}
    </ChipsList>
    {cities.size > 0 && (
      <Search raised onClick={() => search(cities)}>
        Search
      </Search>
    )}
  </Container>
)

Chips.propTypes = {
  // @connect
  cities: PropTypes.instanceOf(List).isRequired,
  onRemove: PropTypes.func.isRequired,
  search: PropTypes.func.isRequired,
}

// This selector wraps the next `connect` to give the `mapDispatchToProps` function
// access to the form values.
const selectFormValues = createStructuredSelector({
  formValues: getFormValues(FORM.NAME),
})

const mapStateToProps = createStructuredSelector({
  cities: selectCities(),
  formValues: getFormValues(FORM.NAME),
})

function mapDispatchToProps(dispatch, props) {
  return {
    onRemove: city => {
      const newFormValue = props.formValues
        .get(FORM.FIELDS.CITIES)
        .replace(city, '')

      // Removes the city from the app state's list
      pipeRight(dispatch, remove)(city)
      // Updates the form
      pipeRight(dispatch, change)(FORM.NAME, FORM.FIELDS.CITIES, newFormValue)
    },
    search: pipeRight(dispatch, list.start),
  }
}

export default pipeRight(
  connect(selectFormValues),
  connect(mapStateToProps, mapDispatchToProps)
)(Chips)
