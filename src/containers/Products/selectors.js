/*
 *
 *  containers/List selectors
 *
 */

import { createSelector } from 'reselect'
import { denormalize } from 'normalizr'
import { List } from 'immutable'

import { makeStandardSelector } from 'utils/selectors'
import { selectDomain as selectEntitiesDomain } from 'services/entities/selectors'
import { product } from 'services/entities/definitions'

import { DOMAIN, STATE } from './constants'

const selectDomain = () => state => state.get(DOMAIN)
const selectResult = () => state =>
  state.getIn([DOMAIN, STATE.STATUS, 'result'])

export const selectStatus = makeStandardSelector(selectDomain())

export const selectProducts = () =>
  createSelector(selectEntitiesDomain(), selectResult(), (entities, list) => {
    return denormalize(List(list), [product], entities)
  })
