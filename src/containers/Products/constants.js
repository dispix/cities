/*
 * 
 * containers/List constants
 *
 */

export const DOMAIN = 'list'
export const ROOT = `containers/${DOMAIN}`
export const STATE = {
  STATUS: 'status',
}
export const RESET = {
  [STATE.STATUS]: `${ROOT}/${STATE.STATUS}/reset`,
}
