/*
 *
 *  containers/List reducer 
 *
 */

import { combineReducers } from 'redux-immutable'

import { makeStandardReducer } from 'utils/reducers'
import { LIST } from 'services/products/constants'

import { RESET, STATE } from './constants'

export default combineReducers({
  [STATE.STATUS]: makeStandardReducer(LIST, RESET[STATE.STATUS]),
})
