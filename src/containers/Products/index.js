import React from 'react'
import PropTypes from 'prop-types'
import { LinearProgress } from 'material-ui/Progress'
import { Map, Iterable } from 'immutable'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'

import ProductsList from 'components/ProductsList'

import { STATE } from './constants'
import { selectStatus, selectProducts } from './selectors'

const Products = ({ status, list }) => (
  <div>
    {status.get('loading') && <LinearProgress />}
    {!!status.get('result') && <ProductsList list={list.toJS()} />}
  </div>
)

Products.propTypes = {
  // @connect
  list: PropTypes.instanceOf(Iterable).isRequired,
  status: PropTypes.instanceOf(Map).isRequired,
}

const mapStateToProps = createStructuredSelector({
  list: selectProducts(),
  status: selectStatus(STATE.STATUS),
})

export default connect(mapStateToProps)(Products)
