/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable'

import * as CONST from './constants'

function removeCity(city) {
  return cities => cities.delete(cities.indexOf(city))
}

// The initial state of the App
const initialState = fromJS({
  [CONST.STATE.CITIES]: [],
})

function appReducer(state = initialState, action) {
  switch (action.type) {
    case CONST.REGISTER:
      return state.set(CONST.STATE.CITIES, action.payload)
    case CONST.REMOVE:
      return state.update(CONST.STATE.CITIES, removeCity(action.payload))
    default:
      return state
  }
}

export default appReducer
