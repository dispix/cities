/*
 *
 *  containers/App actions
 *
 */

import * as CONST from './constants'

export function register(cities) {
  return {
    type: CONST.REGISTER,
    payload: cities,
  }
}

export function remove(city) {
  return {
    type: CONST.REMOVE,
    payload: city,
  }
}
