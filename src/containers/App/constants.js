/*
 *
 *  containers/App constants
 *
 */

export const DOMAIN = 'app'

export const REGISTER = `${DOMAIN}/register`
export const REMOVE = `${DOMAIN}/remove`

export const AVAILABLE_CITIES = [
  'lyon',
  'paris',
  'marseille',
  'toulouse',
  'lille',
]

export const STATE = {
  CITIES: 'cities',
}
