/**
 *
 *  containers/App selectors
 *
 */

import { createSelector } from 'reselect'

import { DOMAIN, STATE } from './constants'

export const selectDomain = () => state => state.get(DOMAIN)

export const selectCities = () =>
  createSelector(selectDomain(), substate => substate.get(STATE.CITIES))
