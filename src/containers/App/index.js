import React from 'react'
import styled from 'styled-components'

import Input from 'containers/Input'
import Chips from 'containers/Chips'
import Products from 'containers/Products'

const Container = styled.div`
  text-align: center;
`
const Header = styled.header`
  background-color: ${({ theme }) => theme.colors.main.primary};
  height: 100px;
  padding: 20px;
  color: white;
`
const Title = styled.h1`
  font-size: 1.5em;
  color: ${({ theme }) => theme.colors.main.text};
`
const Main = styled.main`
  font-size: large;
  padding-top: 8px;
`

const App = () => (
  <Container>
    <Header>
      <Title>Awesome product finder</Title>
    </Header>
    <Main>
      <Input />
      <Chips />
      <Products />
    </Main>
  </Container>
)

export default App
