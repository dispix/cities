/**
 * Combine all reducers in this file and export the combined reducers.
 * If we were to do this in store.js, reducers wouldn't be hot reloadable.
 */

import { combineReducers } from 'redux-immutable'
import { reducer as formReducer } from 'redux-form/immutable'

import globalReducer from 'containers/App/reducer'
import { DOMAIN as APP_DOMAIN } from 'containers/App/constants'
import productsReducer from 'containers/Products/reducer'
import { DOMAIN as PRODUCTS_DOMAIN } from 'containers/Products/constants'
import { reducers as servicesReducers } from 'services'

export default function createReducer() {
  return combineReducers({
    [APP_DOMAIN]: globalReducer,
    [PRODUCTS_DOMAIN]: productsReducer,
    services: servicesReducers,
    form: formReducer,
  })
}
