import 'babel-polyfill'
import 'sanitize.css/sanitize.css'

import React from 'react'
import ReactDOM from 'react-dom'
import { ThemeProvider } from 'styled-components'
import { Provider } from 'react-redux'

import 'global-styles'
import App from 'containers/App'
import registerServiceWorker from 'registerServiceWorker'
import configureStore from 'store'
import theme from 'theme'

const initialState = {}
const store = configureStore(initialState)

const render = () => {
  ReactDOM.render(
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </Provider>,
    document.getElementById('root')
  )
}

// if (module.hot) {
//   module.hot.accept(() => {
//     render()
//   })
// }

render()

registerServiceWorker()
