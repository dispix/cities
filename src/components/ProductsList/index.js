import React from 'react'
import PropTypes from 'prop-types'
import Table, {
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from 'material-ui/Table'

const ProductsList = ({ list }) =>
  list.length === 0 ? (
    <p>No products to show</p>
  ) : (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Ref</TableCell>
          <TableCell>Product</TableCell>
          <TableCell>City</TableCell>
          <TableCell numeric>Price</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {list.map(item => (
          <TableRow key={item.id}>
            <TableCell>{item.ref}</TableCell>
            <TableCell>{item.name}</TableCell>
            <TableCell>{item.city.name}</TableCell>
            <TableCell numeric>${item.price}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  )

ProductsList.propTypes = {
  list: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default ProductsList
