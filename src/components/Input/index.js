import React from 'react'
import PropTypes from 'prop-types'
import TextField from 'material-ui/TextField'

const Input = ({ input }) => <TextField {...input} />

Input.propTypes = {
  input: PropTypes.object.isRequired,
}

export default Input
