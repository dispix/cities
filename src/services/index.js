import { combineReducers } from 'redux-immutable'

// Import reducers
import entitiesReducer from 'services/entities/reducer'

// Import sagas
import productsSagas from 'services/products/sagas'

// Import constants
import { DOMAIN as ENTITIES_DOMAIN } from 'services/entities/constants'

const reducers = combineReducers({
  [ENTITIES_DOMAIN]: entitiesReducer,
})

const sagas = [...productsSagas]

export { reducers, sagas }
