import { schema } from 'normalizr'

import City from 'services/cities/Model'
import Product from 'services/products/Model'
import { ENTITY as CITIES } from 'services/cities/constants'
import { ENTITY as PRODUCTS } from 'services/products/constants'

const city = new schema.Entity(CITIES.toLowerCase(), {
  processStrategy: value => new City(value),
})
const product = new schema.Entity(PRODUCTS.toLowerCase(), {
  processStrategy: value => new Product(value),
})

product.define({ city })

const serviceSchema = {
  [city.key]: [city],
  [product.key]: [product],
}

export { city, product }
export default serviceSchema
