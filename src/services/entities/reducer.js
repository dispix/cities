/*
 *
 * services/entities reducer
 *
 */

import { fromJS } from 'immutable'

import * as PRODUCTS from 'services/products/constants'

import { city, product } from './definitions'

const initialState = fromJS({
  [city.key]: {},
  [product.key]: {},
})

function reducer(state = initialState, action) {
  switch (action.type) {
    case PRODUCTS.LIST.SUCCESS:
      return state.merge(fromJS(action.payload.entities))
    default:
      return state
  }
}

export default reducer
