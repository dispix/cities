/*
 *
 *  services/entities selectors 
 *
 */
import { createSelector } from 'reselect'
import { denormalize } from 'normalizr'

import { DOMAIN } from './constants'

export const selectDomain = () => state => state.getIn(['services', DOMAIN])

export const selectEntities = (type, list) =>
  createSelector(
    selectDomain(),
    state =>
      list
        ? denormalize(list, [type], state)
        : denormalize(state.get(type.key).keySeq(), [type], state)
  )
