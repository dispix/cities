/*
 *
 *  services/products constants 
 * 
 */

import { createStandardConstants } from 'utils/constants'

export const DOMAIN = 'products'
export const ROOT = `services/${DOMAIN}`
export const ENTITY = 'Products'

export const LIST = createStandardConstants(`${ROOT}/LIST`)
