import { normalize } from 'normalizr'
import { call, put, takeLatest, select } from 'redux-saga/effects'

import request from 'utils/request'
import { product } from 'services/entities/definitions'

import { LIST } from './constants'
import { list } from './actions'

function* watcher() {
  yield takeLatest(LIST.START, getProducts)
}

function* getProducts({ meta: { cities } }) {
  let url = 'http://localhost:3001/products'

  if (cities && cities.size > 0) {
    url = cities
      .reduce((str, city) => `${str}&city.name=${city}`, `${url}?`)
      .replace('?&', '?')
  }

  try {
    const response = yield call(request, url)
    const data = normalize(response, [product])
    yield put(list.success(data))
  } catch (e) {
    yield put(list.error(e))
  }
}

export default [watcher]
