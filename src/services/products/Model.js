import { Record } from 'immutable'

import { ENTITY } from './constants'

const empty = {
  ref: null,
  name: null,
  price: null,
  city: null,
}

class Product extends Record(empty, ENTITY) {
  /**
     * Casts the price as a Number
     * 
     * @readonly
     * @memberof Product
     * @return {Number}
     */
  get price() {
    return +this.price
  }
}

export default Product
