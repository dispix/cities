import * as CONST from './constants'

export const list = {
  start(cities) {
    return {
      type: CONST.LIST.START,
      meta: { cities },
    }
  },
  success(payload) {
    return {
      type: CONST.LIST.SUCCESS,
      payload,
    }
  },
  error(error) {
    return {
      type: CONST.LIST.ERROR,
      error,
    }
  },
}
