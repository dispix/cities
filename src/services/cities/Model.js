import { Record } from 'immutable'

import { ENTITY } from './constants'

const empty = {
  id: null,
  name: null,
}

class City extends Record(empty, ENTITY) {}

export default City
