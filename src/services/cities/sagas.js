import fetch from 'whatwg-fetch'
import { normalize } from 'normalizr'
import { call, put, takeLatest } from 'redux-saga/effects'

import { product } from 'services/entities/definitions'

import { LIST } from './constants'
import { list } from './actions'

function* watcher() {
  yield takeLatest(LIST.START, getProducts)
}

function* getProducts() {
  const url = 'http://localhost:3001/products'
  let data

  try {
    const response = yield call(fetch, { url })
    data = normalize(response, [product])
    yield put(list.success(data))
  } catch (e) {
    yield put(list.error(e))
  }
}

export default [watcher]
