/*
 *
 *  The application theme is injected by styled-components in the context and is
 *  used throughout the application
 *
 */

const theme = {
  colors: {
    main: {
      primary: '#42a5f5',
      dark: '#0077c1',
      light: '#80d6ff',
      text: '#000000',
    },
    secondary: {
      primary: '#ab47bc',
      dark: '#790e8b',
      light: '#df78ef',
      text: '#ffffff',
    },
  },
}

export default theme
