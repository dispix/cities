# Cities

## Quick start

### Installation

Clone the project:
```sh
git clone git@gitlab.com:dispix/cities.git
```

Install the dependencies:
```sh
yarn install
```
Or if you do not have `yarn`:
```sh
npm install
```

### Start the development environment

The project works with two scripts: the development server and the json-server API that offers a RESTful API with fixtures.

To start the development server:
```sh
npm run start
```

To start the fixtures REST API:
```sh
npm run db
```

## Commit guidelines

This project follows the spirit of the [Angular commit guidelines](https://github.com/angular/angular.js/blob/master/CONTRIBUTING.md#-git-commit-guidelines).

## Miscellaneous

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).